<!DOCTYPE html>
<html>
<head>
	<title>Data Buku</title>
</head>
<body>

<h1>Data Buku</h1>
<h3>Perpustakaan Clara</h3>

</br>
<a href="/buku/tambah">Input Data Buku</a>

</br>
</br>

<table border="1">
		<tr>
			<th>Id</th>
			<th>Judul</th>
			<th>Penulis</th>
			<th>Penerbit</th>
            <th>Jenis</th>
            <th>Pilihan</th>
		</tr>
		@foreach($buku as $p)
		<tr>
			<td>{{ $p->id }}</td>
			<td>{{ $p->judul }}</td>
			<td>{{ $p->penulis }}</td>
			<td>{{ $p->penerbit }}</td>
            <td>{{ $p->jenis }}</td>
            <td>
                <a href="{{ $p->id }}/edit">Edit</a>
                <form action="/buku/hapus/{{ $p->id }}" method="post">
            @method('delete')
            @csrf
            <button type="submit">Hapus</button>
        </form>
        
		</tr>
		@endforeach
	</table>
 
	<br/>
	Halaman : {{ $buku->currentPage() }} <br/>
	Jumlah Data : {{ $buku->total() }} <br/>
	Data Per Halaman : {{ $buku->perPage() }} <br/>
 
 
	{{ $buku->links() }}


</body>
</html>