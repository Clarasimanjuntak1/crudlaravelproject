<!doctype html>
<html>
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">
        <link href="https://stackpath.bootstrapcdn.com/bootstrap/4.2.1/css/bootstrap.min.css" rel="stylesheet">
        <title>Tambah Data</title>
    </head>
    <body>
        <div class="container">
            <div class="card mt-5">
        
                <div class="card-body">
                    <a href="/buku" class="btn btn-primary">Kembali</a>
                    <br/>
                    <br/>
                    
                    <form method="post" action="/buku/store">

                        {{ csrf_field() }}

                        <div class="form-group">
                            <label>Id</label>
                            <input type="text" name="id" class="form-control" placeholder="">

                            @if($errors->has('id'))
                                <div class="text-danger">
                                    {{ $errors->first('id')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <label>Judul</label>
                            <textarea name="judul" class="form-control" placeholder=""></textarea>

                             @if($errors->has('judul'))
                                <div class="text-danger">
                                    {{ $errors->first('judul')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Penulis</label>
                            <textarea name="penulis" class="form-control" placeholder=""></textarea>

                             @if($errors->has('penulis'))
                                <div class="text-danger">
                                    {{ $errors->first('penulis')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Penerbit</label>
                            <textarea name="penerbit" class="form-control" placeholder=""></textarea>

                             @if($errors->has('penerbit'))
                                <div class="text-danger">
                                    {{ $errors->first('penerbit')}}
                                </div>
                            @endif

                        </div>
                        <div class="form-group">
                            <label>Jenis</label>
                            <textarea name="jenis" class="form-control" placeholder=""></textarea>

                             @if($errors->has('jenis'))
                                <div class="text-danger">
                                    {{ $errors->first('jenis')}}
                                </div>
                            @endif

                        </div>

                        <div class="form-group">
                            <input type="submit" class="btn btn-success" value="Simpan">
                        </div>

                    </form>

                </div>
            </div>
        </div>
    </body>
</html>