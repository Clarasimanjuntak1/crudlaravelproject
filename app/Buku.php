<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Buku extends Model
{
    protected $table = "databuku";

    protected $fillable = ['id', 'judul', 'penulis', 'penerbit', 'jenis'];

    public function categories()
    {
        return $this->hasMany(Category::class);
    }

}
