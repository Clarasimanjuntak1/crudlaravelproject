<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Category extends Model
{
    public function buku(){
        return $this->belongsTo('App\Buku');
    }
    protected $fillable =[
        'buku_id', 'tag'
    ];
}
